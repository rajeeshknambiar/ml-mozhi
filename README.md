# ml-mozhi

Malayalam input method — ‘mozhi’ transliteration scheme.

## Enhancements

This project contains few major changes from [upstream](https://www.nongnu.org/m17n/)
1. Fixes spurious ZWNJ (`U+200C`) in a [few combinations](https://savannah.nongnu.org/bugs/?59681)
2. Joiner chillus by default (ൻ → `n_`). Use double underscore for atomic chillus
3. Mapping for additional characters:
    -    ​ൎ(dotreph) → `R_`
    - ൿ → `k__`
    - ൕ → `y__`
    - ൔ → `m__`
    - ൖ →‌ `zh__`
    - ഽ → `=!`
    - ൄ  → `RRi` or `RR^` 
    - ഌ → `LLLi`
    - ൡ → `LLLI`
    - ൢ → `L^i`
    - ൣ → `L^I`

## Installation

1. Install `m17n-db` if not already
2. `sudo curl -o /usr/share/m17n/ml-mozhi.mim -s https://gitlab.com/rajeeshknambiar/ml-mozhi/-/raw/main/ml-mozhi.mim`
3. Restart `ibus-daemon`, using command `ibus-deamon -dr` or via system tray menu or logout/login.
